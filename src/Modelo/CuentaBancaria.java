/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author bmfil
 */
public class CuentaBancaria {

    private int numeroCuenta;
    private cliente datosCliente;
    private String fechaAper;
    private String nombreban;
    private float porRendi;
    private float saldo;

    public CuentaBancaria(int numeroCuenta, cliente datosCliente, String fechaAper, String nombreban, float porRendi, float saldo) {
        this.numeroCuenta = numeroCuenta;
        this.datosCliente = datosCliente;
        this.fechaAper = fechaAper;
        this.nombreban = nombreban;
        this.porRendi = porRendi;
        this.saldo = saldo;
    }

    public CuentaBancaria() {
        this.numeroCuenta = 0;
        this.datosCliente = null;
        this.fechaAper = "";
        this.nombreban = "";
        this.porRendi = 0.0f;
        this.saldo = 0.0f;
    }

    public CuentaBancaria(CuentaBancaria cuba) {
        this.numeroCuenta = cuba.numeroCuenta;
        this.datosCliente = cuba.datosCliente;
        this.nombreban = cuba.nombreban;
        this.porRendi = cuba.porRendi;
        this.saldo = cuba.saldo;

    }

    public int getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(int numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public cliente getDatosCliente() {
        return datosCliente;
    }

    public void setDatosCliente(cliente datosCliente) {
        this.datosCliente = datosCliente;
    }

    public String getFechaAper() {
        return fechaAper;
    }

    public void setFechaAper(String fechaAper) {
        this.fechaAper = fechaAper;
    }

    public String getNombreban() {
        return nombreban;
    }

    public void setNombreban(String nombreban) {
        this.nombreban = nombreban;
    }

    public float getPorRendi() {
        return porRendi;
    }

    public void setPorRendi(float porRendi) {
        this.porRendi = porRendi;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    public void depositar(float deposi){
        this.saldo += deposi;
    }
    public boolean retirar(float reti){
        if(reti <= this.saldo){
           saldo = saldo- reti;
                return (true);
        }else{
            return(false);
        }
    }
    
    public float calcularRendimiento(){
        return porRendi*saldo/365;
    }
}
