/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

/**
 *
 * @author bmfil
 */
public class dlgcuentaBan extends javax.swing.JDialog {

    /**
     * Creates new form dlgcuentaBan
     */
    public dlgcuentaBan(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup2 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        txtnumCuenta = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtfechaNa = new javax.swing.JTextField();
        rbumasculino = new javax.swing.JRadioButton();
        rbufemenino = new javax.swing.JRadioButton();
        txtNombre = new javax.swing.JTextField();
        txtdomicilio = new javax.swing.JTextField();
        btnnuevo = new javax.swing.JButton();
        btnguardar = new javax.swing.JButton();
        btnmostar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtnombreBan = new javax.swing.JTextField();
        txtfechaAper = new javax.swing.JTextField();
        txtporRendi = new javax.swing.JTextField();
        txtsaldo = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtcant = new javax.swing.JTextField();
        txtnuevoSaldo = new javax.swing.JTextField();
        btndepo = new javax.swing.JButton();
        btnRet = new javax.swing.JButton();
        btncerrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Num. cuenta");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(40, 60, 100, 27);

        txtnumCuenta.setEnabled(false);
        txtnumCuenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnumCuentaActionPerformed(evt);
            }
        });
        getContentPane().add(txtnumCuenta);
        txtnumCuenta.setBounds(140, 70, 130, 22);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Datos del cliente", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Agency FB", 0, 18))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Nombre");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("Domicilio");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("Fecha Nacimiento");

        txtfechaNa.setEnabled(false);

        rbumasculino.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup2.add(rbumasculino);
        rbumasculino.setText("Sexo masculino  ");
        rbumasculino.setToolTipText("");
        rbumasculino.setActionCommand("Sexo masculino");
        rbumasculino.setEnabled(false);
        rbumasculino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbumasculinoActionPerformed(evt);
            }
        });

        rbufemenino.setBackground(new java.awt.Color(204, 204, 204));
        buttonGroup2.add(rbufemenino);
        rbufemenino.setText("Sexo Femenino\n");
        rbufemenino.setEnabled(false);
        rbufemenino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbufemeninoActionPerformed(evt);
            }
        });

        txtNombre.setEnabled(false);

        txtdomicilio.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtdomicilio, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                    .addComponent(txtNombre))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtfechaNa, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(rbufemenino, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(rbumasculino, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE))
                        .addGap(136, 136, 136))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(txtfechaNa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(rbumasculino)
                    .addComponent(txtdomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbufemenino)
                .addContainerGap(10, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1);
        jPanel1.setBounds(30, 120, 510, 150);

        btnnuevo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnnuevo.setText("Nuevo");
        getContentPane().add(btnnuevo);
        btnnuevo.setBounds(570, 70, 140, 50);

        btnguardar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setEnabled(false);
        getContentPane().add(btnguardar);
        btnguardar.setBounds(570, 140, 140, 50);

        btnmostar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnmostar.setText("Mostrar");
        btnmostar.setEnabled(false);
        getContentPane().add(btnmostar);
        btnmostar.setBounds(570, 210, 140, 50);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Nombre del Banco");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(50, 304, 120, 20);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Fecha de Apertura");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(50, 344, 120, 15);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Porcentaje de rendimiento");
        getContentPane().add(jLabel7);
        jLabel7.setBounds(50, 384, 150, 20);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("Saldo");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(50, 424, 120, 20);

        txtnombreBan.setEnabled(false);
        getContentPane().add(txtnombreBan);
        txtnombreBan.setBounds(220, 300, 150, 22);

        txtfechaAper.setEnabled(false);
        getContentPane().add(txtfechaAper);
        txtfechaAper.setBounds(220, 340, 150, 22);

        txtporRendi.setEnabled(false);
        getContentPane().add(txtporRendi);
        txtporRendi.setBounds(220, 380, 40, 22);

        txtsaldo.setEnabled(false);
        getContentPane().add(txtsaldo);
        txtsaldo.setBounds(220, 420, 150, 22);

        jPanel2.setBackground(new java.awt.Color(255, 255, 225));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "::Movimientos::", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Agency FB", 0, 18))); // NOI18N
        jPanel2.setLayout(null);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("Cantidad");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(50, 40, 70, 15);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Nuevo saldo");
        jPanel2.add(jLabel10);
        jLabel10.setBounds(50, 80, 70, 15);

        txtcant.setEnabled(false);
        jPanel2.add(txtcant);
        txtcant.setBounds(150, 30, 120, 22);

        txtnuevoSaldo.setEnabled(false);
        jPanel2.add(txtnuevoSaldo);
        txtnuevoSaldo.setBounds(150, 80, 120, 22);

        btndepo.setText("Hacer Deposito");
        btndepo.setEnabled(false);
        jPanel2.add(btndepo);
        btndepo.setBounds(380, 30, 180, 25);

        btnRet.setText("Hacer retiro");
        btnRet.setEnabled(false);
        jPanel2.add(btnRet);
        btnRet.setBounds(380, 80, 180, 25);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(50, 460, 600, 120);

        btncerrar.setText("Cerrar");
        getContentPane().add(btncerrar);
        btncerrar.setBounds(600, 600, 100, 40);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtnumCuentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnumCuentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnumCuentaActionPerformed

    private void rbumasculinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbumasculinoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbumasculinoActionPerformed

    private void rbufemeninoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbufemeninoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbufemeninoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgcuentaBan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgcuentaBan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgcuentaBan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgcuentaBan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgcuentaBan dialog = new dlgcuentaBan(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnRet;
    public javax.swing.JButton btncerrar;
    public javax.swing.JButton btndepo;
    public javax.swing.JButton btnguardar;
    public javax.swing.JButton btnmostar;
    public javax.swing.JButton btnnuevo;
    public javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JRadioButton rbufemenino;
    public javax.swing.JRadioButton rbumasculino;
    public javax.swing.JTextField txtNombre;
    public javax.swing.JTextField txtcant;
    public javax.swing.JTextField txtdomicilio;
    public javax.swing.JTextField txtfechaAper;
    public javax.swing.JTextField txtfechaNa;
    public javax.swing.JTextField txtnombreBan;
    public javax.swing.JTextField txtnuevoSaldo;
    public javax.swing.JTextField txtnumCuenta;
    public javax.swing.JTextField txtporRendi;
    public javax.swing.JTextField txtsaldo;
    // End of variables declaration//GEN-END:variables
}
