/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Vista.dlgcuentaBan;
import Modelo.CuentaBancaria;
import Modelo.cliente;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class controlador implements ActionListener {

    private CuentaBancaria cuba;
    private dlgcuentaBan vista;
    
    public controlador(CuentaBancaria cuba, dlgcuentaBan vista) {
        this.cuba = cuba;
        this.vista = vista;
        
        //se inicializan los btn
        vista.btnRet.addActionListener(this);
        vista.btndepo.addActionListener(this);
        vista.btnguardar.addActionListener(this);
        vista.btnmostar.addActionListener(this);
        vista.btnnuevo.addActionListener(this);
        vista.btncerrar.addActionListener(this);
    }
    
    //iniciamos vista
    private void iniciarVista() {
        vista.setTitle("Cuenta Bancaria");
        vista.setSize(740, 690);
        vista.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        //btn nuevo
        if (e.getSource() == vista.btnnuevo) {
            vista.btnguardar.setEnabled(true);
            vista.btnmostar.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtdomicilio.setEnabled(true);
            vista.txtfechaAper.setEnabled(true);
            vista.txtfechaNa.setEnabled(true);
            vista.txtnombreBan.setEnabled(true);
            vista.txtporRendi.setEnabled(true);
            vista.txtsaldo.setEnabled(true);
            vista.rbufemenino.setEnabled(true);
            vista.rbumasculino.setEnabled(true);
            vista.txtnumCuenta.setEnabled(true);
        }
        //btn guardar
        if (e.getSource() == vista.btnguardar) {
            try{
              JFrame Frame = new JFrame("Guardado con existo");
            cuba.setDatosCliente((new cliente(vista.txtNombre.getText(),
                     vista.txtfechaNa.getText(),
                     vista.txtdomicilio.getText(), vista.buttonGroup2.getSelection().toString())));
            cuba.setNombreban(vista.txtnombreBan.getText());
            cuba.setNumeroCuenta(Integer.parseInt(vista.txtnumCuenta.getText()));
            cuba.setFechaAper(vista.txtfechaAper.getText());
            cuba.setPorRendi(Integer.parseInt(vista.txtporRendi.getText()));
            cuba.setSaldo(Integer.parseInt(vista.txtsaldo.getText()));   
            JOptionPane.showMessageDialog(vista, "Guardado con Exito");
            
            }catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex2.getMessage());
            }
            //se activa la pestaña de movimientos
            vista.txtcant.setEnabled(true);
            vista.btnRet.setEnabled(true);
            vista.btndepo.setEnabled(true);
            //se muestra el saldo que tiene
            vista.txtnuevoSaldo.setText(String.valueOf(cuba.getSaldo()));
        }
        //btn mostrar
        if(e.getSource()== vista.btnmostar){
            vista.txtnumCuenta.setText(String.valueOf(cuba.getNumeroCuenta()));
            vista.txtNombre.setText(String.valueOf(cuba.getDatosCliente().getNombreCliente()));
            vista.txtdomicilio.setText(String.valueOf(cuba.getDatosCliente().getDomicilio()));
            vista.txtfechaNa.setText(String.valueOf(cuba.getDatosCliente().getFechaNa()));
            vista.txtfechaAper.setText(String.valueOf(cuba.getFechaAper()));
            vista.txtnombreBan.setText(String.valueOf(cuba.getNombreban()));
            vista.txtporRendi.setText(String.valueOf(cuba.getPorRendi()));
            vista.txtsaldo.setText(String.valueOf(cuba.getSaldo()));
        }
        //btn depositar
        if(e.getSource() == vista.btndepo){
            try{
              cuba.depositar((Integer.parseInt(vista.txtcant.getText())));
            vista.txtnuevoSaldo.setText(String.valueOf(cuba.getSaldo()));  
            }catch (NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex.getMessage());
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(vista, "Surgio el siguiente error" + ex2.getMessage());
            }
        }
        //btn ret
        if (e.getSource()== vista.btnRet){
            if((cuba.retirar(Integer.parseInt(vista.txtcant.getText())))==true){
             vista.txtnuevoSaldo.setText(String.valueOf(cuba.getSaldo())); 
            }else{
                JOptionPane.showMessageDialog(vista, "No tienes fondos suficientes");
            } 
        }
        //btn cerrar
         if (e.getSource() == vista.btncerrar) {
            int respuesta = JOptionPane.showConfirmDialog(vista, "¿Quieres salir",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (respuesta == JOptionPane.YES_NO_OPTION) {
                vista.dispose();
                System.exit(0);
            }
        }
    }
    public static void main(String[] args) {
        // TODO code application logic here
        //cliente cli = new cliente();
        CuentaBancaria cub = new CuentaBancaria();
        dlgcuentaBan vista = new dlgcuentaBan(new JFrame(), true);

        controlador contra = new controlador(cub, vista);
        contra.iniciarVista();
    }

}
